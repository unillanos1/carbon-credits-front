import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "../views/Login";
import LandsOwners from "../views/LandsOwners";

const RouterApp = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/landsowners" element={<LandsOwners />} />
      </Routes>
    </Router>
  );
};

export default RouterApp;
