import axios from "axios";
import React from "react";

interface ILandOwner {
  landowner_id: number;
  name: string;
  identification: string;
  phone: string;
}

const LandsOwners = () => {
  const [landsOwners, setLandOwners] = React.useState<ILandOwner[]>([]);

  React.useEffect(() => {
    const FetchLandsOwners = async () => {
      const response = await axios.get(
        import.meta.env.VITE_API_URL + "/landowners",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setLandOwners(response.data);
    };
    FetchLandsOwners();
  }, []);

  return (
    <>
      <section className="container mx-auto p-6 font-mono">
        <div className="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
          <div className="w-full overflow-x-auto">
            <table className="w-full">
              <thead>
                <tr className="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
                  <th className="px-4 py-3">Id</th>
                  <th className="px-4 py-3">Name</th>
                  <th className="px-4 py-3">Identification</th>
                  <th className="px-4 py-3">Phone</th>
                </tr>
              </thead>
              <tbody className="bg-white">
                {landsOwners.map((landOwner: ILandOwner) => (
                  <tr className="text-gray-700">
                    <td className="px-4 py-3 text-ms font-semibold border">
                      {landOwner.landowner_id}
                    </td>
                    <td className="px-4 py-3 border">
                      <div className="flex items-center text-sm">
                        <div className="relative w-8 h-8 mr-3 rounded-full md:block">
                          <img
                            className="object-cover w-full h-full rounded-full"
                            src="https://images.pexels.com/photos/5212324/pexels-photo-5212324.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
                            alt=""
                            loading="lazy"
                          />
                          <div
                            className="absolute inset-0 rounded-full shadow-inner"
                            aria-hidden="true"
                          ></div>
                        </div>
                        <div>
                          <p className="font-semibold text-black">
                            {landOwner.name}
                          </p>
                        </div>
                      </div>
                    </td>
                    <td className="px-4 py-3 text-xs border">
                      <span className="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-sm">
                        {" "}
                        {landOwner.identification}{" "}
                      </span>
                    </td>
                    <td className="px-4 py-3 text-sm border">
                      {landOwner.phone}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </>
  );
};

export default LandsOwners;
